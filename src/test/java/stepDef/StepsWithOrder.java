package stepDef;

import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepsWithOrder {

  private final WebDriver driver = WebDriverManager.getDriverManager().getDriver();
  private final WebDriverWait wait = new WebDriverWait(driver, 10);

  @ParameterType(".*")
  public Order order(String order) {
    return Order.valueOf(order);
  }

  @Step("Выбрана необходимая сортировка")
  @И("в выпадающем списке сортировка выбрано значение {order}")
  public void orderSelect(Order order) {
    String orderPath = "//div[@class=\"sort-select-3QxXG select-select-box-3LBfK select-size-s-2gvAy\"]//select[@class=\"select-select-3CHiM\"]";
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(orderPath)));
    Select ordering = new Select(driver.findElement(By.xpath(
        orderPath)));
    ordering.selectByVisibleText(order.value);
    WebDriverManager.getDriverManager().takeScreenshot(driver);
  }
}
