package stepDef;

public enum Order {
  По_умолчанию("По умолчанию"),
  Дешевле("Дешевле"),
  Дороже("Дороже"),
  По_дате("По дате");
  public String value;

  Order(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
