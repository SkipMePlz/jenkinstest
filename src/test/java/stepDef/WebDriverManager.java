package stepDef;

import io.qameta.allure.Attachment;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;

public class WebDriverManager {

  private static WebDriverManager driverManager = null;
  private final WebDriver driver;

  private WebDriverManager() {
    System.setProperty("webdriver.chrome.driver", "E:\\driver\\chromedriver.exe");
    driver = new ChromeDriver();
    driver.manage().window().maximize();
  }

  public static WebDriverManager getDriverManager() {
    if (driverManager == null) {
      driverManager = new WebDriverManager();
    }
    return driverManager;
  }

  @Attachment(value = "Screenshot", type = "image/png")
  public byte[] takeScreenshot(WebDriver driver) {
    Screenshot screenshot = new AShot().takeScreenshot(driver);
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    try {
      ImageIO.write(screenshot.getImage(), "PNG", buffer);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return buffer.toByteArray();
  }

  public WebDriver getDriver() {
    return driver;
  }

  public void shutdown() {
    driver.quit();
    driverManager = null;
  }
}
