import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumHomework {

  public static void main(String[] args) {
    System.setProperty("webdriver.chrome.driver", "E:\\driver\\chromedriver.exe");
    WebDriver driver = new ChromeDriver();
    driver.manage().window().maximize();
    driver.get("https://www.avito.ru/");
    Select categories = new Select(
        driver.findElement(By.xpath("//select[@name = \"category_id\"]")));
    categories.selectByVisibleText("Оргтехника и расходники");
    WebElement search = driver
        .findElement(By.xpath("//input[@data-marker=\"search-form/suggest\"]"));
    search.sendKeys("Принтер");
    WebElement city = driver
        .findElement(By.xpath("//div[@class=\"main-select-2pf7p main-location-3j9by\"]"));
    city.click();
    driver.findElement(By.xpath("//input[@class=\"suggest-input-3p8yi\"]")).sendKeys("Владивосток");
    WebDriverWait wait = new WebDriverWait(driver, 10);
    wait.until(ExpectedConditions
        .visibilityOfElementLocated(By.xpath("//span//strong[contains(text(),\"Владивосток\")]")))
        .click();
    driver.findElement(By.xpath("//button[@data-marker=\"popup-location/save-button\"]")).click();
    WebElement checkbox = driver
        .findElement(By.xpath("//span[@data-marker=\"delivery-filter/text\"]"));
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("window.scrollBy(0,500)");

    if (!checkbox.isSelected()) {
      checkbox.click();
    }

    driver.findElement(By.xpath("//button[@data-marker=\"search-filters/submit-button\"]")).click();
    Select ordering = new Select(driver.findElement(By.xpath(
        "//div[@class=\"sort-select-3QxXG select-select-box-3LBfK select-size-s-2gvAy\"]//select[@class=\"select-select-3CHiM\"]")));
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
        "//div[@class=\"sort-select-3QxXG select-select-box-3LBfK select-size-s-2gvAy\"]//select[@class=\"select-select-3CHiM\"]")));
    ordering.selectByVisibleText("Дороже");
    wait.until(ExpectedConditions
        .visibilityOfAllElementsLocatedBy(By.xpath("//div[@data-marker=\"item\"]")));
    List<WebElement> printersList = driver.findElements(By.xpath("//div[@data-marker=\"item\"]"));
    for (int i = 0; i < 3; i++) {
      System.out.println("Название: " +
          printersList.get(i).findElement(By.xpath(".//div[@class=\"iva-item-titleStep-2bjuh\"]"))
              .getText()
          + ". Стоимость: " + printersList.get(i)
          .findElement(By.xpath(".//div[@class=\"iva-item-priceStep-2qRpg\"]")).getText());
    }
  }
}
